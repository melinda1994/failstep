import unittest

def setUpRun():
    print("setUpRun")

def tearDownRun():
    print("tearDownRun")

class Model_A(unittest.TestCase):

    def setUpModel(self):
        print("Model_A.setUpModel")
        
    def tearDownModel(self):
        print("Model_A.tearDownModel")

    def vertex_0(self):
        print("Model_A.vertex_0")

    def vertex_1(self):
        print("Model_A.vertex_1")

    def vertex_2(self):
        print("Model_A.vertex_2")

    def edge_0(self):
        print("Model_A.edge_0")

    def edge_1(self):
        print("Model_A.edge_1")


class Model_B(unittest.TestCase):

    def setUpModel(self):
        print("Model_B.setUpModel")

    def tearDownModel(self):
        print("Model_B.tearDownModel")

    def vertex_3(self):
        self.assertTrue(False)
        print("Model_B.vertex_3")

    def vertex_4(self):
        print("Model_B.vertex_4")

    def edge_2(self):
        print("Model_B.edge_2")

    def edge_3(self):
        print("Model_B.edge_3")
